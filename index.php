<?php
session_start();
include_once('mdl/app.php');
$GLOBALS['app'] = new theApp();
$app = $GLOBALS['app'];
require 'vendor/autoload.php';
$loader = new Twig_Loader_Filesystem('views');
$twig = new Twig_Environment($loader);
$response = array();
$controls = @$_SERVER['PATH_INFO'];
$method = $_SERVER['REQUEST_METHOD'];
if ($controls == '') {
    $response['errcode'] = -1;
    $response['errmess'] = 'No Parameter Detected';
    $f[0] = 'noapp';
} else {
    $builder = explode('?', $controls);
    $filters = trim($builder[0], '/');
    $params = @$builder[1];
    $f = explode('/', $filters);
    $response['builder']    = $builder;
    $response['filter']     = $filters;
    $response['params']     = $params;
    $response['f']          = $f;
}

switch ($f[0]) {
    default:
        include('awal.php');
        break;
    case 'beranda':

        echo json_encode($u);
        break;
    case 'cekLogin':
        $response=array();
        $email = $_POST['email'];
        $password= $_POST['pass'];
        $p=$app->getUser($email,$password);

        if($p->id!=''){
            $response['errno']=1;
            $response['errmess']="Berhasil";
            $_SESSION['ID_USER']=$p->id;
            $_SESSION['LEVEL_USER']=$p->level;
            $_SESSION['NM_USER']=$p->nama;
            $_SESSION['picture']=$p->picture;
            $_SESSION['email'] = $email;
            $_SESSION['password'] = $password;
        }else{
            $response['errno']=0;
            $response['errmess']="Gagal";
        }
        echo json_encode($response);
        break;
    case 'logout':
        session_destroy();
        header('location: /');
        break;
    case 'daftar':
        echo $twig->render('register.html', array(
            'path' => $app->assets
         ));
        break;
}
?>