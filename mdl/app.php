<?php
error_reporting(E_ALL);
/**
 * User: San
 * Date: 13/08/2016
 * Time: 17:35
 */
require_once("incs/class.MysqliDatabase.php");

class theApp extends MysqliDatabase
{
    public $server = "system.net";
    public $assets  = "assets";
    private $read;
    private $errorMess = '';
    private $dataCS;
    public $statLogin = false;
    private $idUser;
    private $dbClient;
    private $keyApp='';

    public function __construct()
    {
        include_once('config.php');
        $CS = explode(';', $db_param_reader);
        foreach ($CS as $Key => $Element) {
            $Element = explode('=', $Element);
            if (isset($Element[1])) {
                $CS[trim($Element[0])] = trim($Element[1]);
                unset($CS[$Key]);
            }
        }
        $this->dataCS = $CS;
        @$this->connect(empty($CS['server']) ? 'localhost' : $CS['server'],
            empty($CS['username']) ? 'root' : $CS['username'],
            empty($CS['password']) ? 'okebos123' : $CS['password'],
            empty($CS['database']) ? 'db_twig' : $CS['database'],
            empty($CS['port']) ? 3306 : $CS['port'],
            empty($CS['socket']) ? null : $CS['socket']);
        $this->keyApp=$kunci;
        if ($this->connect_errno) {
            if ($ThrowExceptions) throw new Exception(mysqli_connect_error(), mysqli_connect_errno());
        }
    }
    public function getUser($email,$password)
        {

            $sql = "SELECT * FROM t_user WHERE EMAIL_USER='$email' AND PASSWORD_USER='$password' AND STATUS_USER=1";
            $data = $this->query_one($sql);
            $hasil = new stdClass();
            if ($data) {
                $hasil->id = $data->ID_USER;
                $hasil->nama = $data->NM_USER;
                $hasil->level = $data->TIPE_USER;
                $hasil->picture = "https://journalism.missouri.edu/wp-content/uploads/2011/10/jon-stemmle-731x1024.jpg";
            } else {
                $hasil->id = null;
                $hasil->nama = null;
                $hasil->level = null;
                $hasil->picture = null;
            }
            $hasil->error = $this->error;
            $hasil->errno = $this->errno;
            $hasil->items = $data;
            return $hasil;
        }
//
//    public function getClient()
//    {
//        $ser = $_SERVER['SERVER_NAME'];
//        $sql = "SELECT ID_SIKAD,NM_KLIEN,LOCAL_SETTING FROM mastersikad.t_sikad WHERE URI='$ser'";
//        $data = $this->query_one($sql);
//        $hasil = new stdClass();
//        if ($data) {
//            $hasil->id = $data->ID_SIKAD;
//            $hasil->nama = $data->NM_KLIEN;
//        } else {
//            $hasil->id = null;
//            $hasil->nama = null;
//        }
//        $hasil->error = $this->error;
//        $hasil->errno = $this->errno;
//        $hasil->items = $data;
//        $db = $data->LOCAL_SETTING;
//        if ($db == 'production') $db = 'pas_sma';
//        $this->dbClient = $db;
//        return $hasil;
//    }
//
//    public function setUser($id)
//    {
//        $login = array();
//        $db = $this->dbClient;
//        $sql = "SELECT * FROM $db.v_pegawai_aktif WHERE NOMOR_INDUK_BARU='$id'";
//        $data = $this->query($sql);
//        if ($data) {
//            $this->statLogin = true;
//            $this->idUser = $id;
//            $login['errmess'] = 'Login Berhasil';
//            $login['errno'] = 1;
//        } else {
//            $login['errmess'] = 'Nomor Induk tidak ditemukan';
////            $login['str'] = $sql;
//        }
//        return $login;
//    }
//
    public function getMenu($params) {
        $hasil = new stdClass();
//        $id=$this->idUser;
        $sql="SELECT * FROM t_menu WHERE LEVEL_MENU=2 AND ID_PARENT IS NULL ORDER BY `URUT_MENU`";
        $data=$this->query_all($sql);
        $bread='';
        if($data) {
            foreach ($data as $index => $item) {
                $anak=$this->anakMenu($item->ID_MENU,$params);
                $data[$index]->anak=$anak;
                if($anak->items) {
                    $data[$index]->hiper='javascript:;';
                    $data[$index]->className='treeview';
                    if($anak->isOpen) {
                        $classLi=' active';
                        $bread=$item->NM_MENU;
                    } else $classLi='';
                    $data[$index]->classLi=$classLi;

                } else {
                    $data[$index]->hiper=sha1($this->keyApp.$item->LINK_MENU);
                    $data[$index]->className='treeview';
                    if($params==sha1($this->keyApp.$item->LINK_MENU)) $classLi=' active'; else $classLi='';
                    $data[$index]->classLi=$classLi;
                }
            }
        }
//        $hasil->error = $this->error;
//        $hasil->errno = $this->errno;
        $hasil->items = $data;
        $hasil->bread = $bread;
        return $hasil;
    }

    private function anakMenu($idMenu,$params) {
        $sql="SELECT * FROM t_menu WHERE ID_PARENT=$idMenu ORDER BY `URUT_MENU`";
        $data=$this->query_all($sql);
        $isOpen=false;
        if($data) {
            foreach ($data as $index => $item) {
                $anak=$this->anakMenu($item->ID_MENU,$params);
                $data[$index]->anak=$anak;
                if($anak->items) {
                    $data[$index]->hiper='javascript:;';
                    $data[$index]->className='treeview-menu';
                    if($anak->isOpen) {
                        $classLi=' active';
                        $isOpen=true;
                    } else $classLi='';
                    $data[$index]->classLi=$classLi;
                } else {
                    $data[$index]->hiper=sha1($this->keyApp.$item->LINK_MENU);
                    $data[$index]->className='treeview';
                    if($params==sha1($this->keyApp.$item->LINK_MENU)) {
                        $classLi=' active';
                        $isOpen=true;
                    } else {
                        $classLi='';
                    }
                    $data[$index]->classLi=$classLi;
                }
            }
        }
        $hasil = new stdClass();
        $hasil->error = $this->error;
        $hasil->errno = $this->errno;
        $hasil->isOpen = $isOpen;
        $hasil->items = $data;
        return $hasil;

    }
}

?>